##Some Commands
> node stream-server.js mysecret

> ffmpeg -i rtsp://205.232.36.94/1293364019-low1.sdp -r 24000/1001 -f mpeg1video http://localhost:8082/mysecret/640/480

##Getting the Stream:

1. Open Browser
    * If using Chrome, make sure you have security disabled with `--disable-web-security` option, otherwise you will have issues with CORS headers
1. Login to: https://205.232.36.94/jagwire/login.jsp
2. In the browser, open findStream/index.html
    * Unfortunately, you will need a web server for this at the moment due to CDN libraries (should look at fixing)
    * If python is installed, then running `python -m SimpleHTTPServer` in the repo directory will help
3. You can then choose a profile and use the stream
