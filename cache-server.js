var fs = require('fs');

if(process.argv.length < 3) {
	console.log(
		'Usage: \n' +
		'node stream-server.js <secret> [<stream-port> <websocket-port>]'
	);
	process.exit();
}

var STREAM_SECRET = process.argv[2],
	STREAM_PORT = process.argv[3] || 8083

var width = 640,
	height = 480;

// HTTP Server to accept incomming MPEG Stream
var streamServer = require('http').createServer( function(request, response) {
	var params = request.url.substr(1).split('/');
	width = (params[1] || 640)|0;
	height = (params[2] || 480)|0;

	if(params[0] == STREAM_SECRET) {
		console.log(
			'Stream Connected: ' + request.socket.remoteAddress + 
			':' + request.socket.remotePort + ' size: ' + width + 'x' + height
		);

		handleRequest(request);
	}
	else {
		console.log(
			'Failed Stream Connection: '+ request.socket.remoteAddress + 
			request.socket.remotePort + ' - wrong secret.'
		);
		response.end();
	}
}).listen(8083);

function handleRequest(request) {
	var count = 0;
	var startTime = (new Date()).getTime();
	var out = fs.createWriteStream('cache' + count + '.webm');

	request.on('data', function(data){
		console.log(data.length);
		out.write(data);

		var currentTime = (new Date()).getTime();
		if (currentTime - startTime > 20000) {
			startTime = currentTime;
			count++;
			console.log('closing cache file and creating another');
			out.end();
			out = fs.createWriteStream('cache' + count + '.webm');
		}
	});
	request.on('end', function() {
		console.log('closing buffer');
		out.end();
	});
}

console.log('Listening for MPEG Stream on http://127.0.0.1:8083/<secret>/<width>/<height>');