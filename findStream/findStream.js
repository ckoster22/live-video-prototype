$(document).on('ready', function() {
  var resultJson,
      $contentSelect = $('#content'),
      $profileSelect = $('#profile'),
      $streamUrlInput = $('#stream-url'),
      $ffmpegCommandInput = $('#ffmpeg-command');

      $contentSelect.on('change', function(event) {
        var selectedContent = _.find(resultJson.results, {'content_id': this.value});
        _.each(selectedContent.profiles, function(profile) {
          $('<option>').text(profile.profile_title + ' ' + profile.profile_type).val(profile.profile_id).appendTo($profileSelect);
        });

        $profileSelect.change();
      });

      $profileSelect.on('change', function(event) {
        var selectedContent = _.find(resultJson.results, {'content_id': $contentSelect.val()}),
            selectedProfile = _.find(selectedContent.profiles, {'profile_id': this.value}),
            baseUrl = 'https://205.232.36.94/jagwire/services/search.jsp?',
            paramMap = {
              service: 'media',
              node: selectedContent.node_id,
              mediaType: selectedProfile.profile_type,
              profile: selectedProfile.profile_id,
              identifier: selectedContent.content_id,
              startTime: 0,
              stopTime: 0
            };

            $.ajax({
                url: baseUrl + $.param(paramMap),
                success: function(data) {
                  var streamUrl = JSON.parse(data).mediaUrl,
                      ffmpegCommand = 'ffmpeg -i ' + streamUrl + ' -f mpeg1video http://localhost:8082/mysecret/' + selectedProfile.profile_width + '/' + selectedProfile.profile_height;

                  $streamUrlInput.val(streamUrl);
                  $ffmpegCommandInput.val(ffmpegCommand);
                }
            });
      });

    $.ajax({
        url: 'https://205.232.36.94/jagwire/services/search.jsp?fieldName=STREAMING&fieldValue=true',
        success: function(data) {
          resultJson = JSON.parse(data);

          _.each(resultJson.results, function(resultItem) {
            $('<option>').text(resultItem.summary2).val(resultItem.content_id).appendTo($contentSelect);
          });

          $contentSelect.change();
        }
    });
});
